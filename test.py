# -*- coding: utf-8 -*
import os ,time

def screenshot (name):
	
	os.system( str("adb shell screencap -p /sdcard/"+name+".png"))
	os.system(str("adb pull /sdcard/"+name+".png"))
def back_home_page():
	os.system('adb shell input tap 100 1700')
def last_step():
	os.system('adb shell input tap 100 1850')
def make_xml():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	return xmlString

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	back_home_page()
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Baries & 2. [Screenshot] Side Bar Text
def test_side_baries():
	back_home_page()
	os.system('adb shell input tap 100 100')
	xmlString = make_xml()
	check_list = [
		'查看商品分類',
		'查訂單/退訂退款',
		'追蹤/買過/看過清單',
		'智慧標籤',
		'PChome 旅遊',
		'線上手機回收',
		'給24h購物APP評分']
	for i in check_list:
		assert xmlString.find(i) != -1
	screenshot("side_bar_text")
	last_step()
# 3. [Context] Categories & # 4. [Screenshot] Catego查看商品分類
def test_Categories():
	os.system('adb shell input swipe 100 600 100 500')
	os.system('adb shell input tap 970 1600')
	#os.system('adb shell input tap 1000 300')
	xmlString = make_xml()
	check_list = [
		'3C',
		'周邊',
		'NB',
		'通訊',
		'食品',
		'生活',
		'運動戶外',
		'美妝',
		'生活',
		'運動戶外',
		'美妝',
		'生活',
		'運動戶外',
		'美妝',
		'衣鞋包錶'
		]
	for i in check_list:
		assert xmlString.find(i) != -1
	screenshot("Categories")

# 5. [Context] Categories page & 6. [Screenshot] Categories page
def test_Categories_page():
	back_home_page()# 3. [Context] Categories

	os.system('adb shell input tap 400 1750')
	xmlString = make_xml()
	check_list = [
		'3C',
		'周邊',
		'NB',
		'通訊',
		'數位',
		'家電',
		'日用',
		'食品',
		'生活',
		'運動戶外',
		'美妝',
		'衣鞋包錶'
		]
	for i in check_list:
		assert xmlString.find(i) != -1
	screenshot("Categories_page")
# 

# 7. [Behavior] Search it Text
def test_search_text():
	back_home_page()
	os.system('adb shell input tap 600 150')
	time.sleep(3)
	os.system('adb shell input text "switch"') 
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(5)
	xmlString = make_xml()

	assert xmlString.find("Switch") != -1
	screenshot("search_in_text")


# 8. [Behavior] Follow an item and it should be add to the list
def test_follow_add_to_the_list():
	os.system('adb shell input tap 300 600')
	time.sleep(3)
	os.system('adb shell input tap 100 1750') #PREES HEART
	time.sleep(3)
	last_step()
	time.sleep(3)
	os.system('adb shell input tap 100 1750')
	time.sleep(3)
	os.system('adb shell input tap 100 100')
	time.sleep(3)
	os.system('adb shell input tap 100 800')
	xmlString = make_xml()
	assert xmlString.find("Switch") != -1
	screenshot("follow＆add_to_the_list")

# 9. [Behavior] Navigate tto the detail of item
def test_detail_of_item():
	os.system('adb shell input tap 300 600')
	time.sleep(2)
	os.system('adb shell input swipe 100 600 100 5')
	time.sleep(2)
	os.system('adb shell input tap 600 100')
	screenshot("test_detail_of_item")
	xmlString = make_xml()
	assert xmlString.find("商品特色") != -1
	screenshot("detail_of_item")
# 10. [Screenshot] Disconnetion Screen
def test_disconnect():
	os.system('adb shell svc data disable')
	os.system('adb shell svc wifi disable')
	screenshot('disconnection')
	os.system('adb shell svc data enable')
	os.system('adb shell svc wifi enable')

	